const express = require('express');
const app = express();
const settings = require('./settings');
const router = express.Router();
const routes = require('./routes');
const mysql = require('mysql');

const connection = mysql.createConnection(settings.database);

router.get('/users', routes.users.getUsers);

app.use('/api', router);

connection.connect(error => {
  if (error) {
    console.error(`Error connecting to the database:`, error);
    return process.exit();
  }

  app.locals.connection = connection;
  app.listen(settings.APIServerPort, () => {
    console.info(`Server is listening on ${settings.APIServerPort}. `);
  });
});
